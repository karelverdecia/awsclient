from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
    name='awsclient',
    version=version,
    description="Python client to amazon web services.",
    long_description="""\
""",
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Karel Antonio Verdecia Ortiz',
    author_email='kverdecia@gmail.com',
    url='',
    license='',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    include_package_data=True,
    zip_safe=False,
    install_requires=["argparse", "boto",],
    entry_points = {
        'console_scripts': [
            'awsclient-elastic=awsclient.cmd:ElasticIpCmd.run',
            'awsclient-private=awsclient.cmd:PrivateIpCmd.run',
            'awsclient-assigned=awsclient.cmd:AssignedIpCmd.run',
            'awsclient-list=awsclient.cmd:ListInstancesCmd.run',
            'awsclient-show=awsclient.cmd:ShowInstanceCmd.run',
            'awsclient-start=awsclient.cmd:StartInstanceCmd.run',
            'awsclient-stop=awsclient.cmd:StopInstanceCmd.run',
            'awsclient-assign=awsclient.cmd:AssignIp.run',
        ],
        }
)
