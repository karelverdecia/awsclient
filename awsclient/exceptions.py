# -*- coding: utf-8 -*-

class AWSException(Exception):
    """Base class for all `awsclient` exceptions.
    """


class InstanceNotFound(AWSException):
    """Raised when trying to use a wrong instance id.
    """


class IpNotFound(AWSException):
    """Raised when trying to use a wrong ip address.
    """


class PermissionError(AWSException):
    """Raised when trying to execute an action you do not have permission to execute.
    """


class ConnectionError(AWSException):
    """Raised when an error is returned trying to connect to aws.
    """
    
    
class IpAssigned(AWSException):
    """Raised when trying to assing to an instance an ip addres that is already
    assigned to other instance.
    """


class InstanceStopped(AWSException):
    """
    Raised when executing an operations that requires that an instance is stopped.
    """
