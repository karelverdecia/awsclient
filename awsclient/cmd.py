# -*- coding: utf-8 -*-
import os
import argparse
import abc
import json
from .instances import Instances
from .exceptions import AWSException, InstanceNotFound


PARENT_PARSER = argparse.ArgumentParser(add_help=False)
AUTH_GROUP = PARENT_PARSER.add_argument_group('Authentication')
AUTH_GROUP.add_argument('-r', '--region', action='store')
AUTH_GROUP.add_argument('-a', '--access-key', action='store')
AUTH_GROUP.add_argument('-s', '--secret-key', action='store')


class Command(object):
    __metaclass__ = abc.ABCMeta

    parser = argparse.ArgumentParser(parents=[PARENT_PARSER])

    @property
    def region(self):
        arguments = self.parser.parse_args()
        if arguments.region:
            return arguments.region
        if 'AWSCLIENT_REGION' in os.environ:
            return os.environ['AWSCLIENT_REGION']
        return 'us-west-2'

    @property
    def access_key(self):
        arguments = self.parser.parse_args()
        if arguments.access_key:
            return arguments.access_key
        if 'AWSCLIENT_ACCESS_KEY' in os.environ:
            return os.environ['AWSCLIENT_ACCESS_KEY']
        msg = "You must specify the access key id either in the --access-key argument or in the AWSCLIENT_ACCESS_KEY "" \
        ""environment variable."
        self.parser.error(msg)

    @property
    def secret_key(self):
        arguments = self.parser.parse_args()
        if arguments.secret_key:
            return arguments.secret_key
        if 'AWSCLIENT_SECRET_KEY' in os.environ:
            return os.environ['AWSCLIENT_SECRET_KEY']
        msg = "You must specify the secret key id either in the --secret-key argument or in the AWSCLIENT_SECRET_KEY "" \
        ""environment variable."
        self.parser.error(msg)

    def create_client(self):
        return Instances(self.region, self.access_key, self.secret_key)

    @classmethod
    def run(cls):
        cmd = cls()
        cmd()

    @abc.abstractmethod
    def __call__(self):
        pass


class IpListCmd(Command):
    @abc.abstractmethod
    def get_ip_list(self):
        return []

    def __call__(self):
        for ip in self.get_ip_list():
            print ip


class ElasticIpCmd(IpListCmd):
    parser = argparse.ArgumentParser(
        description="Show elastic ip addresses",
        parents=[PARENT_PARSER])
    group = parser.add_argument_group("Filtering options")
    group.add_argument('filter', action='store', default='all', choices=['all', 'available', 'assigned'])

    def get_ip_list(self):
        arguments = self.parser.parse_args()
        client = self.create_client()
        client.elastic_ip_list(arguments.filter)


class PrivateIpCmd(IpListCmd):
    parser = argparse.ArgumentParser(
        description="Show private ip addresses",
        parents=[PARENT_PARSER])

    def get_ip_list(self):
        client = self.create_client()
        client.private_ip_list()


class AssignedIpCmd(IpListCmd):
    parser = argparse.ArgumentParser(
        description="Show all assigned ip addresses",
        parents=[PARENT_PARSER])

    def get_ip_list(self):
        client = self.create_client()
        client.assigned_ip_list()


class ListInstancesCmd(Command):
    parser = argparse.ArgumentParser(
        description="Show all instances' metadata.",
        parents=[PARENT_PARSER])

    def __call__(self):
        client = self.create_client()
        print json.dumps(list(client))


class ShowInstanceCmd(Command):
    parser = argparse.ArgumentParser(
        description="Show metadata of an instance.",
        parents=[PARENT_PARSER])
    parser.add_argument('instance_id', action='store', help="Id of the instance to show.")

    def __call__(self):
        arguments = self.parser.parse_args()
        client = self.create_client()
        try:
            print json.dumps(client[arguments.instance_id])
        except InstanceNotFound:
            msg = "There is no available instance with id {0}".format(arguments.instance_id)
            self.parser.error(msg)


class StartInstanceCmd(Command):
    parser = argparse.ArgumentParser(
        description="Starts an instance.",
        parents=[PARENT_PARSER])
    group = parser.add_argument_group("Execution params")
    group.add_argument('instance_id', action='store', help="Id of the instance to be started.")
    group.add_argument(
        '-w', '--wait', action='store_true', default=False,
        help="Wait until the instance have been fully started.")

    def __call__(self):
        arguments = self.parser.parse_args()
        client = self.create_client()
        try:
            print client.start(arguments.instance_id, arguments.wait)
        except InstanceNotFound:
            msg = "There is no available instance with id {0}".format(arguments.instance_id)
            self.parser.error(msg)


class StopInstanceCmd(Command):
    parser = argparse.ArgumentParser(
        description="Starts an instance.",
        parents=[PARENT_PARSER])
    group = parser.add_argument_group("Execution params")
    group.add_argument('instance_id', action='store', help="Id of the instance to be stopped.")
    group.add_argument(
        '-w', '--wait', action='store_true', default=False,
        help="Wait until the instance have been fully stopped.")

    def __call__(self):
        arguments = self.parser.parse_args()
        client = self.create_client()
        try:
            print client.stop(arguments.instance_id, arguments.wait)
        except InstanceNotFound:
            msg = "There is no available instance with id {0}".format(arguments.instance_id)
            self.parser.error(msg)


class AssignIp(Command):
    parser = argparse.ArgumentParser(
        description="Assign an elastic ip address to an instance.",
        parents=[PARENT_PARSER])
    parser.add_argument(
        'instance_id', action='store',
        help="Id of the instance to which you are assigning the ip address")
    parser.add_argument(
        'elastic_ip', action='store',
        help="Elastic ip address that will be assigned to the instance.")
    parser.add_argument(
        '--start', action='store_true', default=False,
        help="If the instance is not running starts it.")

    def __call__(self):
        arguments = self.parser.parse_args()
        client = self.create_client()
        try:
            client.assign_ip(arguments.instance_id, arguments.elastic_ip, arguments.start)
            params = arguments.elastic_ip, arguments.instance_id
            print "The elastic ip {0} have been associated to the instance {1}".format(*params)
        except AWSException, e:
            msg = "Error assigning ip: {0}".format(e)
            arguments.error(msg)
