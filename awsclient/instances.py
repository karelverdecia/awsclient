# -*- coding: utf-8 -*-
import time
import boto.ec2
from .exceptions import *


class Instances(object):
    """Class for handling the execution of Amazon EC2 instances.
    """

    def __init__(self, region, access_key_id, secret_access_key):
        """
        Initialize a connection to amazon.

        Parameters
        ----------
        region: str
            AWS region.
        access_key_id: str
            Access key.
        secret_access_key: str
            Secret access key.
        """
        try:
            self.region = region
            self.conn = boto.ec2.connect_to_region(
                region,
                aws_access_key_id=access_key_id,
                aws_secret_access_key=secret_access_key
            )
        except Exception as e:
            msg = "Error trying to connect to aws: {0}"
            raise ConnectionError(msg.format(e))
        
    def __iter__(self):
        """Iterate by the instance's metadata. Each element returned by this iterator will be a
        dictionary with the same structure of the result of the method `__getitem__`.
        """
        reservations = self.conn.get_all_instances()
        for reservation in reservations:
            for instance in reservation.instances:
                yield {
                    'name': instance.tags.get('Name', ''),
                    'id': instance.id,
                    'public_ip': instance.ip_address,
                    'private_ip': instance.private_ip_address,
                    'dns': instance.dns_name,
                    'state': instance.state,
                }
    
    def __getitem__(self, instance_id):
        """Returns instance's metadata using the instance id to identify the instance
        
        Parameters
        ----------
        instance_id: str
            Id of the instance.
            
        Returns: dict
            Returns a dictionary with the metadata of the instance. Keys
            in the dictionary are:
            
            name: str
                Instance's name.
            id: str
                Instance's ID.
            public_ip: str
                Instance's public ip.
            private_ip: str
                Instance's private ip.
            dns: str
                Instance's domain name.
            state: str
                Execution state.
        """
        for item in self:
            if item['id'] == instance_id:
                return item
        raise KeyError(instance_id)
    
    def __contains__(self, instance_id):
        """Returns if there is an instance with the specified id.
        
        Parameters
        ----------
        instance_id: str
            Id of the instance.
            
        Returns: bool
            Returns `True` if there is an instance the id specified in the
            parameter `instance_id` of `False` otherwise.
        """
        for item in self:
            if item['id'] == instance_id:
                return True
        return False

    def instance_id_list(self):
        """Returns the ids of the available instances (running or not).
        """
        return [item['id'] for item in self]

    def assigned_ip_list(self):
        """
        Returns a list of all ip addresses assigned to instances. This may include elastic
        and not elastic ip addresses.
        """
        return sorted([item['public_ip'] for item in self if item['public_ip'] if item['public_ip']])

    def elastic_ip_list(self, status='all'):
        """Returns a list of the public ips.

        Parameters
        ----------
        status: string
            Filters the ip addresses in the result list. The possible values are:

                all: returns all elastic ip addresses.
                available: returns the elastic ip addresses are not assigned to an instance.
                assigned: returns the elastic ip addresses assigned to instances.

        Returns: list of str
            Returns a list of elastic ip addresses. This list if filtered by the value
            in the `status` parameter.

        Raises
        ------
        ValueError
            Returns this exception if the value of the parameter `status` is incorrect. See the description
            of this parameter.
        """
        result = sorted([address.public_ip for address in self.conn.get_all_addresses()])
        if status == 'all':
            return result
        assigned = self.assigned_ip_list()
        if status == 'assigned':
            return [ip for ip in result if ip in assigned]
        if status == 'available':
            return [ip for ip in result if ip not in assigned]
        raise ValueError("The value of argument status must be all, available or assigned.")

    def private_ip_list(self):
        """
        Returns a list of private ips.
        """
        return sorted([item['public_ip'] for item in self if item['private_ip'] if item['private_ip']])

    def start(self, instance_id, wait=False):
        """
        Starts an instance.
        
        Parameters
        ----------
        instance_id: str
            Id of the instance
        wait: bool
            Indicates if the method must block until the instance has been 
            started.
            
        Raises
        ------
        InstanceNotFound
            If the parameter instance is not the id of any of the available instances.
        """
        if instance_id not in self:
            raise InstanceNotFound(instance_id)
        self.conn.start_instances([instance_id])
        if wait:
            time.sleep(0.1)
            instance = self[instance_id]
            while instance['state'] != 'running':
                time.sleep(0.1)
                instance = self[instance_id]

    def stop(self, instance_id, wait=False):
        """
        Stops an instance.
        
        Parameters
        ----------
        instance_id: str
            Id of the instance
        wait: bool
            Indicates if the method must block until the instance has been 
            started.
            
        Raises
        ------
        InstanceNotFound
            If the parameter instance is not the id of any of the available instances.
        """
        if instance_id not in self:
            raise InstanceNotFound(instance_id)
        self.conn.stop_instances([instance_id])
        if wait:
            time.sleep(0.1)
            instance = self[instance_id]
            while instance['state'] != 'stopped':
                time.sleep(0.1)
                instance = self[instance_id]
    
    def assign_ip(self, instance_id, public_ip, start_instance=False):
        """
        Assign a public ip to an instance. If the instance is not running starts
        it and assign the ip when the instance is already running.
        
        Parameters
        ----------
        instance_id: str
            The instance id.
        public_ip: str
            The public ip to assign to the instance.
        start_instance: bool
            If the instance is not running starts it.
        
        Raises
        ------
        IpAssigned
            - If the ip is already assigned to other instance.
            - If the instance has other public ip assigned and the value of
              parameter `force` is `False`.
        InstanceNotFound
            If there is not an instance with the specified ip.
        IpNotFound
            If the specified Ip is not in the public ips.
        """
        if public_ip not in self.elastic_ip_list('available'):
            raise IpAssigned("The specified ip (%s) is not an available elastic ip." % public_ip)
        try:
            instance = self[instance_id]
        except KeyError:
            raise InstanceNotFound("There is no available instance with id %s." % instance_id)
        if instance['public_ip'] == public_ip:
            return
        if instance['public_ip'] in self.elastic_ip_list('assigned'):
            raise IpAssigned("The instance %s already has an elastic ip assigned." % instance_id)
        if instance['state'] != 'running':
            if start_instance:
                self.start(instance['id'], True)
            else:
                raise InstanceStopped("The instance is not running.")
        self.conn.associate_address(instance_id, public_ip)
